---
title: 'Travelling'
date: '2021-02-22'
---

I have always enjoyed travelling. Travelling helps me go on adventures where I meet new people and experience so many wonderful, new things. Some of the places to which I would love to travel are :
- **Korea** - This would always be my dream destination. The movies I have watched throughout these past few years sowed in me a passion to travel to Korea and experience their culture.
- **France** - Visiting France and tasting the French cuisine is another dream of mine.
Apart from these, there are so many more wonderful places that I would love to explore and visit. I hope i get to be there soon.

“THE WORLD IS A BOOK AND THOSE WHO DO NOT TRAVEL READ ONLY A PAGE.” 
~ SAINT AUGUSTINE
