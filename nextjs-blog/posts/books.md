---
title: 'Books'
date: '2021-02-22'
---

I've loved reading ever since I was a child. Reading takes me to worlds where I am carefree and happy. I laugh along with my favorite characters when they laugh and cry when they cry. Reading opens doors where my imagination is let free and I get to meet so mnay wonderful fictional friends who will always stay close to my heart no matter how old I get. Here are some of my favorite books :

- **A thousand splendid suns** - Khaled Hosseini
- **The Harry Potter series** - J. K. Rowling
- **Inferno** - Dan Brown
- **Kite Runner** - Khaled Hosseini
- **The Pilgrim's Progress** - John Bunyan
- **Little Women** - Louisa May Alcott
- **Pride and Prejudice** - Jane Austen
- **Wuthering Heights** - Emily Bronte

These are but few of my favorite tales. I will always be grateful to these tales for comforting me whenever I felt down and for being my best friends throughout my life.

“You can never get a cup of tea large enough or a book long enough to suit me.” ~ C.S. Lewis